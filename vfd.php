<?php

class vfd {

	private $port;
	private $vfd_size = 40;

	function __construct($port = "/dev/ttyS0") {
		$this->port = $port;
	}

	private function writeToDevice($data) {
		file_put_contents($this->port, str_pad(substr($data, 0, $this->vfd_size), $this->vfd_size, " "));
	}

	public function clear() {
		$this->writeToDevice("");
	}

	public function set($text = "") {
		$this->clear();
		$this->writeToDevice($text);
	}

	public function place(array $content) {
		$count = count($content);
	
		$halves[0] = array_slice($content, 0, $count / 2);
		$halves[1] = array_slice($content, $count / 2);
		$out = "";
		foreach($halves as $half) {
			$field = 20 / ($count / 2);

			$outhalf = "";
			foreach($half as $entry) {
				$outhalf .= str_pad($entry, $field, " ", STR_PAD_BOTH);
			}
			$out .= str_pad(substr($outhalf, 0, 20), 20, " ");

		}
		$this->set($out);
	}

}
