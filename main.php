#!/usr/bin/php
<?php

require_once "vfd.php";

$v = new vfd();

$data = json_decode(file_get_contents("http://erso/statistics.php"), true);

$v->place(array(
	"Ta teden", date("j. n. Y H:i")
));

sleep(20);

$v->place(array(
	$data["gear"]["donation"]["computers"]." doniranih PC", $data["gear"]["qc-working"]["computers"]." pripravljenih PC"
));

sleep(20);

$v->place(array(
	$data["workHours"]["workHours"]." opravljenih ur", $data["workHours"]["users"]." prostovoljcev"
));
